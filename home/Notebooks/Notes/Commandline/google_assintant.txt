Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-10-13T15:42:56-07:00

====== google assintant ======
Created Thursday 13 October 2016

ROM: WETA ROM 9.2
Kernel: Stock
Root: SuperSU 2.78 SR1 (Baked into ROM)
Suhide: v0.55 (0.54 Baked in ROM, I manually flashed update)
SafetyNet Request: Success
Response Validation: Success
CTS Profile Match: True
BusyBox by @osm0sis (Baked into ROM)
Google App version: 6.6.14.21.arm64

Below I'll post literally every step taken to get where I am;

-Navigated to WETA ROM OP
-Downloaded bootloader & radio zip for new update & vendor IMG.
-Installed BL/Radio & Vendor
-Wiped /system, /data, /cache & /dalvik via TWRP
-Flashed WETA 9.2 & booted system.
-Downloaded SuperSU 2.78 SR1 & suhide 0.55
-Reboot recovery, flash suhide 0.55 followed by SuperSU 2.78 SR1
-Set up ROM. Pattern, fingerprint, Wifi, paired my Moto 360, allow unknown sources, dev options / USB debug, smart lock, OK Google voice recognition, etc. 
-Installed suhide GUI from Play Store
-Side note: I did NOT add ANY extra apps to the GUI app. Flashing suhide will automatically hide any and all packages that currently identify root and trigger SafetyNet to fail. 
-Downloaded SafetyNet Checker app, passed.
-Downloaded Snapchat. Was able to log in.
-Downloaded Pokemon GO, was able to play.
-Found this thread, lurked for a little to see how you all did first

-Added "ro.opa.eligible_device=true" to the bottom of my build.prop (no quotation marks)
-Changed "ro.product.model=Nexus 6P" to "ro.product.model=Pixel" in my build.prop (Again, no quotes)
-Went to Settings / Apps / Google. Selected Storage. Clear Cache. Manage Space. Erase Everything. 
-Reboot. Voila! Assistant. 

Notes: I edited my build.prop while booted into Android on the very same phone. I used Solid Explorer to make the edits. I only use "Pixel" and not "Pixel XL". I already set up Google voice detection before attempting to do this. All smart unlock options function 100% correctly and I've tested them each 1 by 1. Moto 360 as a trusted device. Trusted place, my home. Trusted face works. I can UNLOCK the phone by saying OK Google, which now triggers assistant. After I got Assistant the 1st time, I switched product model back to Nexus 6P. It remained for a while, but eventually reverted to Now On Tap. I changed it back to Pixel and wiped Google settings again, rebooted and it is back. OK Google works from literally any screen. In Spotify when the music is playing, I kinda have to yell OK GOOGLE so it can hear me over the music and it does. I can tell it to pause Spotify or turn the volume down. It'll do either. It works from screen off, even if it's been over an hour and it does not have to be plugged in for that to work. What else? Oh, I scratched my nose 3 times while editing the build.prop & I was wearing my lucky underwear when I gave this a shot. I recommend you doing the same... Your lucky underwear though, not mine. 

That's all the relevant information I can think of now.


I can confirm this method is working on my Nexus 6 with Pure Nexus nougat ROM.

Special Note:
In your Settings>Google>Search & Now>Voice primary language must be English(UK)
This was mentioned by another person in this thread that English(US) language file has something missing related to hotword detection.
Tested this between English(US) and English(UK) and i can confirm hotword detection breaks the moment English(US) is set to primary. Reverting back to English(UK) and repeating the steps above had it working for me again.
Whether it would work with other languages is unknown to me.



Here's how I got it to work. Initially I kept getting Google Now-On-Tap Cards instead of Google Assistant and my 6P wouldn't trigger the Assistant:

Add the "device_eligible" config to build.prop
Change model to Pixel XL (IMPORTANT)
Google App - Clear all data
Clear all recents
Reboot device
Open Google App and do initial setup.

If NowOnTap still shows, then switch launcher, force stop Google App, clear data and switch back to GNL/PixelLauncher - (Google app probably preloaded something with your default launcher during boot)
Setup Google Now the second time.
Long press Home.
Should work now.

(And yes, as the OP stated, you need to be running 7.0 to do this. Which means you're either on stock rom, or a very early build of a Nougat custom ROM).




Woot!!! Got it to work on my N6, stock ROM rooted (UK English language... US not attempted)

I went back to original build.prop and trained the voice model in GN
Then I re-edited it, rebooted, and DID NOT clear the app data (I would think that doing so wipes the trained model)
Waited about 5-10 minutes (to see if it took time for the build.prop changes to take effect), long press and still had now on tap. 

* Instead of clearing the data, I rebooted the phone again*

After the 2nd reboot, I now have assistant on long press AND OK Google detection works.

Would like to see more people try this and confirm they get the same results. Again, I did not clear the app data, just rebooted a 2nd time after waiting 5-10 minutes (not even sure if the wait is required)


I can confirm the "okay google" detection gets fixed, once you revert to google now and train the voice model and again revert to the google assistant.
Heres exactly what i did.
1. Enolled in the google app beta (you do this by going in to the playstore, browsing to the google app screen, scrolling down and select i'm in for beta esting)
2. Added the "ro.opa.eligible_device=true" line to the build.prop
-changed product model to Pixel XlL
3.cleared the app data of the google app. Go to settings >apps >google app > manage storage > clear all data and click ok.
4.rebooted and waited a few minutes.
Presssand hold the home button the google assistant comes up, but the train voice screen continiously comes up when you say "okay google" so..
5. Go the build.prop again
-delete the "ro.opa.eligible_device=true" line
-renamed the product model to what ever it was before
Save and reboot.
6. Once it reboots you should have google now again, go to settings of the google app, go in to "okay google detection" tap always on and train your voice model.
7. Go in to build.prop again add "ro.opa.eligible_device=true" line
Change the product model to Pixel XL.
Save and reboot
DONE ! (Once you reboot, i would suggest waiting 5 minutes so itagain recognises the build prop changes) it should now trigger the assistant when you say okay google.
I am using nexus 6 with stock rom rooted btw.
