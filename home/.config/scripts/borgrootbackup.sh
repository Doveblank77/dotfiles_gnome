!/bin/bash

# Backup all of /home and /var/www except a few
# excluded directories
# Backup the root filesystem into an archive named "root-YYYY-MM-DD"
# use zlib compression (good, but slow) - default is no compression
borg create -v --stats /home/thomas/acd/Thomas/Devices/Desktop/Arch-linux/Backups/incremental/Arch-borg::root-{now:%Y-%m-%d} / --one-file-system